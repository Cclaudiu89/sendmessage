package com.sendmessage.database;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.sendmessage.Person;

public class Database {

	// using C R U D strategy
	// the HEADERS is the title for each row
	private final String[] HEADERS = { "Id", "Name", "RegistrationNumber", "ExpDate", "PhoneNumber", "Car_Brand",
			"E-mail" };

	public void createFile(List<Person> personList) throws IOException {
		try {
			FileWriter out = new FileWriter("src/main/resources/ITP_members.csv");
			CSVPrinter printer = CSVFormat.DEFAULT.withHeader(HEADERS).print(out);
			for (Person person : personList) {
				printer.printRecord(person.getId(), person.getName(), person.getRegistrationNumber(),
						person.getExpDate(), person.getPhoneNumber(), person.getCar_Brand(), person.getEmail());
			}
			printer.flush();
			printer.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Person> readFile() throws IOException {
		List<Person> personDB = new LinkedList<>();
		Reader in = new FileReader("src/main/resources/ITP_members.csv");
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader(HEADERS).withFirstRecordAsHeader().parse(in);
		for (CSVRecord record : records) {
			int id = Integer.parseInt(record.get("Id"));
			String name = record.get("Name");
			String regNumber = record.get("RegistrationNumber");
			String expDate = record.get("ExpDate");
			double phoneNumber = Double.parseDouble(record.get("PhoneNumber"));
			String car_Brand = record.get("Car_Brand");
			String email = record.get("E-mail");
			Person person = new Person(id, name, regNumber, expDate, phoneNumber, car_Brand, email);
			personDB.add(person);
		}

		return personDB;
	}

	public void updateFile(int id, Person updatePerson) throws IOException {
		List<Person> tempList = readFile();
		for (Person person : tempList) {
			if (person.getId() == id) {
				person.setName(updatePerson.getName());
				person.setRegistrationNumber(updatePerson.getRegistrationNumber());
				person.setExpDate(updatePerson.getExpDate());
				person.setPhoneNumber(updatePerson.getPhoneNumber());
				person.setCar_Brand(updatePerson.getCar_Brand());
				person.setEmail(updatePerson.getEmail());
			}
		}
		createFile(tempList);
	}
	
	public void deleteEntry(int id) throws IOException{
		List<Person> tempList = readFile();
		List<Person> newList = new LinkedList<>();
		for(Person person: tempList) {
			if(person.getId() != id) {
				newList.add(person);
			}
		}
		createFile(newList);
	}
	
	public void addPerson(Person newPerson) throws IOException{
		List<Person> newList = readFile();
		newList.add(newPerson);
		createFile(newList);
	}

}
