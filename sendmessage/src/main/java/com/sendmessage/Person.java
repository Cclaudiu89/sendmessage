package com.sendmessage;

public class Person {

	private int id;
	private String name;
	private String registrationNumber;
	// expDate, I have to change it in a DateTime, data type,
	// when I will read it, from the file.
	private String expDate;
	private double phoneNumber;
	private String car_Brand;
	private String email;
	
	public Person(int id, String name, String registrationNumber, String expDate, double phoneNumber, String car_Brand, String email) {
		this.setId(id);
		this.name = name;
		this.registrationNumber = registrationNumber;
		this.expDate = expDate;
		this.phoneNumber = phoneNumber;
		this.car_Brand = car_Brand;
		this.setEmail(email);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public double getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(double phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCar_Brand() {
		return car_Brand;
	}

	public void setCar_Brand(String car_Brand) {
		this.car_Brand = car_Brand;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
