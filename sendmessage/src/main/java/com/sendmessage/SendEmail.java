package com.sendmessage;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class SendEmail {
	
	private final String[] HEADERS = { "Id", "Name", "RegistrationNumber", "ExpDate", "PhoneNumber", "Car_Brand",
	"E-mail" };
	
	public List<Person> readFile() throws IOException {
		List<Person> personDB = new LinkedList<>();
		Reader in = new FileReader("src/main/resources/ITP_members.csv");
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader(HEADERS).withFirstRecordAsHeader().parse(in);
		for (CSVRecord record : records) {
			int id = Integer.parseInt(record.get("Id"));
			String name = record.get("Name");
			String regNumber = record.get("RegistrationNumber");
			String expDate = record.get("ExpDate");
			double phoneNumber = Double.parseDouble(record.get("PhoneNumber"));
			String car_Brand = record.get("Car_Brand");
			String email = record.get("E-mail");
			Person person = new Person(id, name, regNumber, expDate, phoneNumber, car_Brand, email);
			personDB.add(person);
		}

		return personDB;
	}
	
	public void sendMessage() throws IOException{
		List<Person> personList = readFile();
		
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		
		try {
		for(Person person: personList) {
			DateFormat personDate = new SimpleDateFormat("yyyy-MM-dd");
			Date expDate = personDate.parse(person.getExpDate());
			Calendar personCalendar = Calendar.getInstance();
			calendar.setTime(expDate);
			int personDay = personCalendar.get(Calendar.DAY_OF_MONTH);
			int personMonth = personCalendar.get(Calendar.MONTH);
			int personYear = personCalendar.get(Calendar.YEAR);
			if(day == personDay + 7) {
				if(year == personYear) {
					if(month == personMonth) {
						// and here, I will send the E-mail.
					}
				}
			}
		}
		}catch(ParseException e) {
			e.printStackTrace();
		}
	}

}
